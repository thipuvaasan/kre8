# kre8
## ( Create Kubernetes => Create K8 => Kre8 )

Create a Kubernetes cluster and setup full GitLab Auto Dev Ops pipelines "on premise" (using 1+ ssh-able unix VMs) for full CI/CD of GitLab repositories
(GitLab can be self-hosted "on premise" as well, optional)

![screenshot](pipeline.png)

# Kubernetes Overview
- high level intro _excellent_ primer diagram and overview of concepts, history, rationale:
- https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/
- https://coreos.com/kubernetes/docs/latest/services.html
- FYI: google states that Kubernetes supports clusters up to 100 nodes with 30 pods per node, with 1–2 containers per pod.  Typical Docker use case are 5-10+ containers/host.

# Kubernetes - Moar Documention:
- https://kubernetes.io/docs/getting-started-guides/kubeadm/
- https://kubernetes.io/docs/user-guide/kubectl-cheatsheet/
- https://kubernetes.io/docs/tutorials/stateless-application/run-stateless-application-deployment/
- https://blog.giantswarm.io/getting-started-with-a-local-kubernetes-environment/


# Requirements for Creating your Kubernetes cluster:
- One or more **linux** nodes you can `ssh` to and have `sudo` privs
    - Assume **ubuntu**, other distros possible with minor adjustments
    - Presently using **xenial** - bionic should be doable
    - Can coexist with other roles/services
    - Installation will **turn off swap** (when present)
      - Requirement to run Kubernetes
      - re-setup box to not start swap on reboot (if applicable) for automated recovery
        - (otherwise you'll have to `sudo swapoff -a` manually after reboots, etc.)
- mac laptop
    - others possible with minor adjustments
    - `brew` package manager (for `kubernetes-cli` and `kubernetes-helm`)
    - assumes `bash` or `zsh`

# Overview
- Uses `kubeadm` to setup kubernetes cluster
- Requires some basic environment variables based config
- Tries to interactively ask only when needed
- Persistent Volumes:
  - Uses 'local storage' for 1 and 2-node K8 clusters
  - Uses Ceph/Rook for 3+ node K8 clusters


---
# Create a K8 cluster as needed
- Start out and sort out your environment variables:
```bash
git  clone  https://gitlab.com/internetarchive/kre8
cd kre8
source ./aliases
```

- You will *probably love* trying this, which highlights cluster changes as it gets going, in a separate terminal window:
```bash
source ./aliases  &&  kall
```
- You can `CTL-c` when the highlighted area saturates too much
- You can `CTL-z` &&  `kill %1` to quit entirely

- Now create your cluster
```bash
./create-kubernetes-cluster.sh
```

---
# Set environment variable - for shell commands below:
  - pick your 'alpha' repo - the one you care the most about and want the most stats for free :)
  - `URL=https://${HOSTNAME-FOR-GITLAB?}/${GROUP?}/${REPOSITORY?}`
  - Examples:
  - `URL=https://gitlab.com/internetarchive/kre8`
  - `URL=https://git.archive.org/services/pyhi`

---
# Setup First Group
- To setup a _group-level_ integration with a K8 cluster/server, go to the group page, eg:
```bash
open  $URL/..
```
- Find the left-hand nav and the `[Kubernetes]` section
- Press the `[Add Kubernetes cluster]` button
- Hit the `[Add existing cluster]` tab

### Kubernetes cluster name:
enter something unique (it doesn't seem to matter too much)

### API URL:
```bash
fgrep server ~/.kube/config |cut -f2- -d: |tr -d ' '
```

### CA Certificate:
```bash
fgrep certificate-authority-data ~/.kube/config |cut -f2 -d: |tr -d ' ' |base64 --decode
```

### Token:
```bash
kre8/create-token.sh
```

### RBAC-enabled cluster:
make sure this _checkbox_ is checked

### GitLab-managed cluster:
check this checkbox

## ENV VARS
- Visit group's [Settings] [CI/CD] [Variables]
- Set POSTGRES.. to `false` to avoid an unused PG service _and more importantly_ unused Persistent Volumes (a given project can later override this to `true` in _their_ settings, if they want to make use of the free/empty PostGres DB)
- [optional] Disable slow code quality test

| key | value
| --- | ---
| POSTGRES_ENABLED | false
| CODE_QUALITY_DISABLED | true

- `[Save variables]`

---
# Setup Applications
- Find the group's `[Kubernetes]` button on the left menu and find the cluster
- _Do each of these one-at-a-time and wait for each to finish_
- Do the `Helm Tiller` `[Install]` setup _first_
  - IFF find DNS-related issues and `kubectl describe ..` is showing 'loop' issues:
    - `ssh $(k8-master) cat /etc/resolv.conf`
    - note a `nameserver` IP address value that is _not_ `127.0.0.1`
    - `kubectl edit -nkube-system cm/coredns`
      - change line from: `proxy . /etc/resolv.conf`
      - to: `proxy . [IP-ADDRESS]`
- Do `GitLab Runner` button `[Install]` next
  - Makes K8 be used for CI/CD tasks
- `Ingress` button `[Install]`
  - If hosting K8 cluster on premise, it will not auto-fillout the IP address
    - dont worry ;-)
- `Cert-Manager` button `[Install]`
- `Prometheus` button `[Install]`

## Setup Ingress
- (load balancer on crack 8-)
- Run this _after_ you've setup the 'Operations: Kubernetes: Applications:' 'Ingress' (right above)
- Allows us to open https://[automatically-created-DNS-name]/ from outside
- _NOTE_: you can add additional K8 (non-master) nodes IP addresses here for redundancy if desired
  - you can then make multiple 'A Name' DNS records for each Kubernetes 'review app' names
```bash
K8_INGRESS_IP=$(k8-ingress-ip)
kubectl -n gitlab-managed-apps patch svc/ingress-nginx-ingress-controller -p='{"spec":{"externalIPs":["'${K8_INGRESS_IP?}'"]}}'
```

### useful url:
https://gitlab.com/help/user/project/clusters/index.md#installing-applications



---
# SELECT YOUR "alpha" / most important gitlab code repo for your new K8 cluster
- Only the alpha will be able to get the cool graphs, easily/immediately, so _choose wisely_ 8-)
- Go to [Operations] side menu for 'alpha' / primary repo you want to be the first (for monitoring/graphs)
```bash
open  $URL
```

### in project's [Settings] [CI/CD] [Auto DevOps]:
- check `Enable Auto DevOps` for repo (if not already enabled)
- `[Save changes]`



---
# Sample very simple/fast Dockerfile for your repo to get a full pipeline running
- you can replace it later
- strongly suggested so you can observe the _full_ CI/CD `Auto DevOps` end-to-end
- Add `helo world` Dockerfile  and full ADO pipeline
```bash
open  $URL/new/master
```
- In the upper left `[Template]` section, select `Dockerfile`
- change `[File name]` just under that to `Dockerfile`
- Paste contents and commit:
```bash
FROM nginx:alpine
RUN  sed -i 's/http {/http { server { listen 5000; }/'  /etc/nginx/nginx.conf
RUN  mkdir -p             /etc/nginx/html
RUN  echo 'hai whirld ' > /etc/nginx/html/index.html
CMD  [ "nginx", "-g", "daemon off;" ]
```
- By default, GitLab repo-based webapps assume it serves http (not https) on port `5000`
  - Strongly suggest trying to adapt your webapp port to `5000` (like above `Dockerfile`)
    - Since K8 will automatically check for traffic on port 5000
      - and kill your container after X seconds of nonresponses -- assuming it has gone AWOL

### Make simple CI/CD `test` phase
- This demonstrates `herokuish` and semi-automatic testing
```bash
open  $URL/new/master
```
- `[File name]` test.js
- copy into contents main area then commit:
```bash
window.hello = 'world'
```
```bash
open  $URL/new/master
```
- `[File name]` package.json
- copy into contents main area then commit:
```bash
{ "devDependencies": {
    "eslint": ""
  },
  "scripts": {
    "test": "node_modules/.bin/eslint ."
  }
}
```
```bash
open  $URL/new/master
```
- `[File name]` .eslintrc.json
- type in `{}` into contents main area then commit



---
# Pipelines and DNS
```bash
open  $URL/pipelines
```
- After a full pipeline run
  - see bottom of `review` phase for _non-master_ branch automatically made website (AKA 'review app') hostname.
- Your master-branch production website location is fixed/simpler:
  - You will get an url for your CI/CD created website:
    - `cd` to _your new_ code repo that you've created
```bash
`source [path-to-cloned-kre8-repo]/aliases
kapp
```
- You will want to assign a DNS mapping for that website hostname to point to `$K8_INGRESS_IP`
- If you would like to temporarily see it (just from your laptop), you can consider:
```bash
echo "${K8_INGRESS_IP?}  ${THAT_HOSTNAME_FROM_ABOVE?}" |sudo tee -a /etc/hosts
```

---
# https certs and ingress info
- GitLab ADO will use the **ingress** to:
  - receive incoming traffic over https
  - accept or reject traffic to a given hostname - depending if project has active pipeline, etc.
  - relay https traffic to webapp pods/services using _http_ (default port 5000)
  - the `Cert-Manager` button `[Install]` above sets up the https certs _automatically_ for you via Lets Encrypt

---
# ADD SECOND REPO TO GROUP
- Open the first repo's `[Settings]` `[CI / CD]`: `[Runners]` section
- Note the 'Runners activated for this project' _number_ (easiest to remember) that is activated/working/green, eg `#123`
- Go to you second repo's `[Settings]` `[CI / CD]`: `[Runners]` section
- enable that same runner
- If you have a stuck pipeline, `cancel` it then `retry` it (and the runner now activated for this project should pick it up and run)

---
# ADD ANOTHER GROUP TO SAME K8 CLUSTER
- Enter the same Kubernetes cluster details from your first group (at the side-menu: `[Operations]` `[Kubernetes]`; then '[Add Kubernetes cluster]' button (top right); then 'Add existing cluster').
- Use the same `[Settings]` `[CI / CD]`: `[Variables]` as your 1st group.
- So far, so good.
- Go to the runners page for an existing project in that group, or the group itself.
- You probably won't see the runner you're using in the 1st group.
- Pipelines sit stalled; no runner to be found.
- **[The Dodge part]**:
- Make a _new_ repo for your 2nd group.
  - It seems in that repo you'll see the runners you can see from the 1st group as available.
  - Pick the runner you normally use/want for this k8 cluster (ie: in the 1st group) for this new repo.
  - Run pipelines - you're good!
- Now go back to another existing repo in the 2nd group runner page
  - the same one showing nothing or gibberish runners before.
- Now you should see the runner(s) that the new repo you just made above as available.
  - Seems like could be a user-level visibility for a new repo calculation or something.
  - But seems to have worked twice now..


# Other Reading
## Possible way to setup multiple 'services' in same build/deploy..
- https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#how-to-use-other-images-as-services
