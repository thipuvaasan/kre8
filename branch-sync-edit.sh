#!/bin/zsh -e

# Script that allows a nice hookin (esp.) to VSCode 'sync-rsync' package and extends an
# editor [file save] to  'sync updated file to branch review container' (when applicable)
#
# This allows quick (< seconds!) visualization and f/b for changes _without_ having to do
# a full commit and push (and wait for CI/CD pipeline).
#
# This is in addition to whatever 'sync-rsync' would normally do
# (typically rsync a [file save] to home.archive.org)
#
# It uses `kubectl` (and not much else! just some glue..) to figure out where to send the file
# and `kubectl cp` ultimately.
#
# You can use this by installing 'sync-rsync' w/ basic 'sync on save' setup; install kubectl;
# and modify your VSCode settings like so:
#    brew install kubernetes-cli
#    "sync-rsync.executable": "/Users/tracey/kre8/branch-sync-edit.sh",
#
# You should have a ~/.kube/config file or similar and this should already do something interesting:
#    kubectl get pods --all-namespaces
#
# If you happen to have a podspec with multiple containers, it will write to the first container.


mydir=${0:a:h}

function main () {
  source $mydir/aliases # for 'branch2pod'
  setup "$@"
}


function setup() {
  # adjust to wherever `kubectl` lives on laptop
  PATH="$PATH:/usr/local/bin"


  if [ "$#" -ge 4 ]; then
    if $(echo "$@" |fgrep -qv .vscode); then
      exit  # this doesnt look right, bail
    fi
    sync-to-pod "$@"  ||  sync-normal "$@"
  elif [ "$#" -eq 1 ]; then
    # single source filename variant -- try to directly find and sync to pod
    SYNC_MASTER_PRODUCTION=1
    sync-to-pod $(realpath "$1")
    exit 0
  elif [ "$#" -eq 2 ]; then
    # single source filename variant -- try to directly find and sync to pod
    BRANCH="$1" # NOTE: may or may not be set
    SYNC_MASTER_PRODUCTION=1
    sync-to-pod $(realpath "$2")
    exit 0
  else
    echo "

Usage: $ME [sync-rsync rsync arguments]
  For example, when using VSCode.
  This will 'switch' on master branch vs. other branches and is designed to run as a single
  filename 'sync-to-pod upon file save' helper.

Usage: $ME [source filename]
  When invoker is \`cd\`-ed somewhere in a git repo (at any branch).
  This is useful to copy a single source file into the relevant pod.

Usage: $ME [branch name override] [source filename]
  When invoker is \`cd\`-ed somewhere in a git repo (at any branch).
  This is useful to copy a single source file into the relevant pod.

"
    exit 1
  fi
}


function sync-normal() {
  # rsyncs file up as vscode extension intended
  rsync "$@"
}


function sync-to-pod() {
  # fish out the file name from what VSCode 'sync-rsync' package sends us -- should be 2nd to last arg
  echo
  FILE=$(echo "$@" |rev |tr -s ' ' |cut -f2 -d' ' |rev)

  if [ "$FILE" = ""  -o  ! -e "$FILE" ]; then
    exit 1 # nothing to do / cautiously exit  :-)
  fi

  # our repository name is normally (and really _should_ be!) our namespace name
  DIR=$(dirname "$FILE")
  [ "$BRANCH" = "" ]  &&  BRANCH=$(git -C "$DIR" rev-parse --abbrev-ref HEAD)

  # now split the FILE name into two pieces -- 'the root of the git tree' and 'the rest'
  TOP=$(git -C "$DIR" rev-parse --show-toplevel)
  REST=$(echo "$FILE" | perl -pe "s|^$TOP||; s|^/+||;")

  # switch dirs so 'k8-namespace-cwd' works
  cd "$DIR"
  POD=$(branch2pod $BRANCH)
  cd -

  NAMESPACE=$(cat /tmp/NS-$USER)

  for var in FILE DIR TOP REST BRANCH NAMESPACE POD; do
    echo $var="${(P)var}"
  done

  # if not in a branch (and not overriding for master) - nothing to do!  :-)
  #                     (and continue on to sync-normal, if relevant)
  [ "$BRANCH" = "master" ] && [ "$NAMESPACE" = "petabox" ] && [ ! $SYNC_MASTER_PRODUCTION ] && return -1

  if [ "$POD" = "" ]; then
    echo 'has this branch run a full pipeline and deployed a Review App yet?'
    exit 1 # no relevant pod found - nothing to do
  fi


  # enough talk!  do it!
  echo
  set -x
  kubectl -n "$NAMESPACE"  cp "$FILE" "$POD":"$REST"
  set +x
  return 0
}


ME="$0"
main "$@"
