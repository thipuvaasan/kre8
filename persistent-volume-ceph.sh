#!/bin/zsh -e

# credit to @dvd for sorting out Ceph/Rook as a replacement to the prior
# glusterfs (which he _also_ sorted out ;-)

set +x
mydir=${0:a:h}
source $mydir/aliases
set -x

unset  KUBECONFIG


# Derived from:
#   https://github.com/rook/rook/blob/master/Documentation/helm-operator.md

# NOTE: if PV gets all messed up (eg: try to remove a node from k8 cluster (!!)) then the
# entire PV can be *trashed* and rebuilt with _something_ like (did pretty close to this apr2018):
#   kubectl delete -n rook-ceph cephblockpool replicapool
#   kubectl delete storageclass rook-ceph-block
#   kubectl -n rook-ceph delete cephcluster rook-ceph
#   kubectl -n rook-ceph get cephcluster
#      remove /var/lib/rook on all k8 nodes
#   kubectl delete namespace rook-ceph
#   kubectl delete namespace rook-ceph-system
#
#
# Derived from:
#   https://rook.io/docs/rook/v0.9/ceph-teardown.html


# Create a ServiceAccount for Tiller in the `kube-system` namespace
kubectl --namespace kube-system create sa tiller

# Create a ClusterRoleBinding for Tiller
kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller


# Patch Tiller's Deployment to use the new ServiceAccount
set +x
echo -n '

You need to hook up the kubernetes cluster in GitLab repo GUI *FIRST*
and then press the [Helm/Tiller] setup button first.

☸️  Enter "yes" to continue or CTL-C: '
read cont
set -x
[ "$cont" = "yes" ]  ||  exit 1

helm init --wait

kubectl --namespace kube-system patch deploy/tiller-deploy -p '{"spec": {"template": {"spec": {"serviceAccountName": "tiller"}}}}'



# Set up rook to manage persistent storage.
# This configuration creates Ceph OSDs on every available node in the cluster.
# It then automatically provisions virtual block devices on demand.
# These volumes are presented to Kubernetes containers independently of location.
helm repo add rook-stable https://charts.rook.io/stable
helm repo update
set +x
echo -n '

Now wait for things to stablize with `kallr-pvr` (from the "aliases" file in this repo) and
☸️  Enter "yes" to continue or CTL-C: '
read cont
set -x
[ "$cont" = "yes" ]  ||  exit 1
helm install --namespace rook-ceph-system rook-stable/rook-ceph




set +x
echo -n '

You need to now wait for pods to be running.
Suggest `kall-pvr`, and when everything seems happy,
☸️  Enter "yes" to continue or CTL-C: '
read cont
set -x
[ "$cont" = "yes" ]  ||  exit 1


kubectl create -f $mydir/rook-cluster.yaml


# We need to reduce to 2 pieces if we only have 2 workers
NUMBER_WORKERS=$(k8-workers |wc -l)
FILTER=ignored-by-default
[[ "$NUMBER_WORKERS" -lt 3 ]]  &&  FILTER='size: 3'

# This is almost entirely boilerplate from the rook.io examples,
# but specifying $PV_DIR as our default storage location.
# This directory must exist on every VM.
# (The rook.io folks plan to make these “recipes” available as helm charts in the future.)
cat $mydir/rook-storageclass.yaml |perl -pe "s/$FILTER/size: 2/" |kubectl create -f -


# This setting makes Rook the default provider to automatically provision volumes
# when a new container needs them.  This is most specifically immediately useful for setting up
# storage for Prometheus to persist performance metrics / graph data.
kubectl patch storageclass rook-ceph-block -p '{
  "metadata": {
    "annotations": {
      "storageclass.kubernetes.io/is-default-class": "true"
    }
  }
}'


# Monitor via:
#   kubectl -n rook-ceph port-forward svc/rook-ceph-mgr-dashboard 7000
#   open http://localhost:7000

set +x
echo '

☸️  NOW wait for all pods to install/stabilize, and then install [Prometheus] in GitLab, etc...
(This takes awhile for everything to setup and stabilize)
'
