#!/bin/zsh -e

set +x

# Useful for setting up a 1 or 2-node K8 cluster when you just want the fastest way
# to working setup and make the single required PV needed for Prometheus just use
# a local dir on the K8 pod-runner for the needed PVC

# helpful links:
#   https://kubernetes.io/docs/concepts/storage/volumes/#local
#   https://kubernetes.io/blog/2018/04/13/local-persistent-volumes-beta/


mydir=${0:a:h}
source $mydir/aliases
set -x


PVNAME=gitlab-prom-pv




echo '
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: local-storage
  annotations:
    storageclass.kubernetes.io/is-default-class: "true"
provisioner: kubernetes.io/no-provisioner
reclaimPolicy: Delete
volumeBindingMode: WaitForFirstConsumer
' |kubectl create -f -

echo "
apiVersion: v1
kind: PersistentVolume
metadata:
  name: ${PVNAME?}
spec:
  storageClassName: local-storage
  capacity:
    storage: 10Gi
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
  volumeMode: Filesystem
  local:
    path: ${PV_DIR?}
  nodeAffinity:
    required:
      nodeSelectorTerms:
      - matchExpressions:
        - key: kubernetes.io/hostname
          operator: NotIn
          values:
          - effectively-ignored-anti-match-value-hostname
" |kubectl create -f -
