#!/bin/zsh -e

# _WIPES OUT_ a kubernetes cluster -- all data, nodes, _EVERYTHING_.
# So you can start the master and nodes over.
# DONT RUN THIS SCRIPT ON A NODE IN YOUR CLUSTER :)
# run this from your laptop

mydir=${0:a:h}


function main() {
  source $mydir/aliases

  env |egrep 'K8|KUBECONFIG' |cat

  kubeadm-reset-start

  # You can do either or both.  Second is _very_ comprehensive and fastest (BUT WIPES OUT ALL DOCKER)
  # kubeadm-reset-slow
  kubeadm-reset-fully
}


function kubeadm-reset-start() {
  echo -n "


CROSS CHECK THIS WITH BELOW - ABOUT TO WIPE OUT:

"
  kubectl --kubeconfig=${KUBECONFIG} get nodes

  echo -n "


THIS WIPES OUT YOUR ENTIRE KUBERNETES CLUSTER AT:

"
  k8-master
  k8-workers
  echo -n "

☸️  Enter [yes] here or [CTL-C] to abort: "
  read cont
  if [ "$cont" != "yes" ]; then
    echo "WISE CHOICE - BAILING."
    exit 1
  fi


  echo -n "

DO YOU WANT TO REMOVE ENTIRELY YOUR PERSISTENT VOLUME AT:

    ${PV_DIR?}

☸️  Enter [yes] here to remove prior/current ${PV_DIR?}: "
  read PVKILL
  if [ "$PVKILL" = "yes" ]; then
    # SUBTLE! need to wipe any /var/lib/rook/ for a new cluster rebuild - since we're including
    # removal of prior (ceph, when applicable) PV
    PVKILL="${PV_DIR?} /var/lib/rook/"
  else
    PVKILL=""
  fi

  set -x

  for ho in $(k8-workers); do
    echo "=== $ho ================================================================================"
    touch /tmp/$ho.log
    set +e
    # NOTE: often (but not required) KUBECONFIG is not set and will default to default location..
    ssh -n $(k8-master) "
kubectl --kubeconfig=${KUBECONFIG} drain $ho --delete-local-data --force --ignore-daemonsets;
kubectl delete node $ho;
" >> /tmp/$ho.log 2>&1 &
    set -e
  done
  wait
}


function kubeadm-reset-slow() {
  for ho in $(k8-master; k8-workers); do
    ssh -n ${ho?} "
echo y |sudo kubeadm reset; sudo reboot;
" >> /tmp/$ho.log 2>&1 &
  done
  wait

  sleep 60
}


# a much more virulent reset -- wipes all docker libraries and images for a _complete_ restart
function kubeadm-reset-fully() {
  HOSTS=$(k8-master; k8-workers)
  for cmd in reboot hostname background-var-lib-docker-remover; do
    for ho in $(echo $HOSTS); do
      touch /tmp/$ho.log
      echo "=== $cmd $ho =========================================================================="
      set +e
      if [ $cmd != "background-var-lib-docker-remover" ]; then
        ssh -n ${ho?} "
sudo service docker stop;
sudo apt-get purge -y --autoremove  --allow-change-held-packages  kubelet  kubeadm  kubectl  kubernetes-cni;
sudo find  ${PVKILL?}  /var/lib/dockershim  /var/lib/kubelet  /var/lib/etcd  /etc/kubernetes -delete;
sudo mv /var/lib/docker /var/lib/nix;
sudo $cmd;
" >> /tmp/$ho.log 2>&1 &
      else
        # this can take forever so we background it...
        ssh -n ${ho?} 'nohup sudo find /var/lib/nix -delete >| /tmp/rm-var-lib-docker.log 2>&1 &'
      fi
      set -e
    done
    wait
    echo "=== PASS $cmd $ho DONE =================================================================="
    sleep 60
  done
}


main
