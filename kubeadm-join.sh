#!/bin/zsh -e


# Joins a node into a kubernetes cluster, using industry-std recommended `kubeadm`.
# Please see `create-kubernetes-cluster.sh` in this dir -- that's usually how it should be done
# (which calls this).

mydir=${0:a:h}
source $mydir/aliases

ho=${1:?"Usage: kubeadm-join.sh <hostname -- not master>"}

set -x

unset  KUBECONFIG


# joins a non-master node into cluster
# NOTE: if fails (24h limited window), simply run:
#    ssh $(k8-master) sudo kubeadm token create
# for a new one and rerun this method on node! :)
MASTER=$(k8-master)
IP=$(IP $MASTER)
TOK=$(ssh $MASTER sudo kubeadm token list |fgrep -v '<invalid>' |egrep . |tail -1 |cut -f1 -d ' ')


# (esp.) if docker is 'too new' give user a chance to manually inspect errors and (interactively)
# tell us to retry ignoring failures or bail
typeset -a ARGS # array
ARGS=
# can try uncommenting various args if doesnt breeze through:
ARGS+=(--discovery-token-unsafe-skip-ca-verification)
# ARGS+=(--ignore-preflight-errors=SystemVerification)
# ARGS+=(--ignore-preflight-errors=FileContent--proc-sys-net-bridge-bridge-nf-call-iptables)
# ARGS+=(--ignore-preflight-errors=SystemVerification,FileContent--proc-sys-net-bridge-bridge-nf-call-iptables)
ssh $ho sudo kubeadm join --token $TOK $IP:6443  $ARGS


# NOTE: if fails with:
#   sysctl: cannot stat /proc/sys/net/bridge/bridge-nf-call-iptables:
# solution:
#   sudo modprobe br_netfilter
