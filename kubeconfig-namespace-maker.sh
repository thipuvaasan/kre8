#!/bin/bash -e

# Creates a kubeconfig file that limits access to a kubernetes cluster to just a single namespace,
# using Kubernetes RBAC system.

# Adapted from:
#   https://jeremievallee.com/2018/05/28/kubernetes-rbac-namespace-user.html

function main() {
  set -x
  echo $NS
  OUTFILE=$HOME/.kube/$NS

  create-service-account-with-permissions
  create-config

  set +x
  echo "

  successfully created $OUTFILE"
}


function create-service-account-with-permissions() {
  echo "
apiVersion: v1
kind: ServiceAccount
metadata:
  name: ${NS?}-user
  namespace: ${NS?}

---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: ${NS?}-user-full-access
  namespace: ${NS?}
rules:
- apiGroups: [\"\", \"extensions\", \"apps\"]
  resources: [\"*\"]
  verbs: [\"*\"]
- apiGroups: [\"batch\"]
  resources:
  - jobs
  - cronjobs
  verbs: [\"*\"]

---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1beta1
metadata:
  name: ${NS?}-user-view
  namespace: ${NS?}
subjects:
- kind: ServiceAccount
  name: ${NS?}-user
  namespace: ${NS?}
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: ${NS?}-user-full-access
" |kubectl create -f -
}


function create-config() {
  SECRET=$(kubectl -n${NS?} get sa/${NS?}-user -o "jsonpath={.secrets[0].name}")
        TOKEN=$(kubectl -n${NS?} get secret/${SECRET?} -o "jsonpath={.data.token}"      | base64 -D)
  CERTIFICATE=$(kubectl -n${NS?} get secret/${SECRET?} -o "jsonpath={.data['ca\.crt']}")
  API=$(kubectl cluster-info |grep -F 'Kubernetes master' |grep -Eo 'https://[a-z0-9:\.]+')

  rm -fv $OUTFILE
  echo -n "apiVersion: v1
kind: Config
preferences: {}
clusters:
- cluster:
    certificate-authority-data: ${CERTIFICATE?}
    server: $API
  name: my-cluster
users:
- name: ${NS?}-user
  user:
    as-user-extra: {}
    client-key-data: ${CERTIFICATE?}
    token: ${TOKEN?}
contexts:
- context:
    cluster: my-cluster
    namespace: ${NS?}
    user: ${NS?}-user
  name: ${NS?}
current-context: ${NS?}
" > $OUTFILE
  chmod 400 $OUTFILE
}


if [ "$1" = "" ]; then
  echo "

Usage: $0 <namespace>

"
  exit 1
else
  NS="$1"
  main
fi
