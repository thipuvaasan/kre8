#!/bin/zsh -e

# Sets up basic requirements for a Kubernetes cluster node (both master and workers).
# Run this from your laptop.

mydir=${0:a:h}
source $mydir/aliases

# what kubernetes version shall we be? (you can also leave these blank strings for 'most recent')
VER="=1.13.0-00"
VER=""     # use most recent
VER_CNI="=0.6.0-00"
VER_CNI="" # use most recent

function main() {
  ho=${1:?"<hostname>"}

  # ensure docker
  (ssh $ho docker -v 2>/dev/null)  ||  (
    set +x
    echo
    echo '☸️  docker seems not installed -- enter [RETURN] to install docker-ce else CTL-C to abort'
    read cont
    set -x
    cat $mydir/install-docker-ce.sh | ssh $ho bash -ex
  )

  set +e
  ssh $ho sudo service docker start  # try to ensure docker is running
  set -e


  # https://kubernetes.io/docs/getting-started-guides/kubeadm/
  #
  # NOTE: Feb2018+ K8 started requiring swap be off
  # NOTE: xfsprogs is required for ceph to mount xfs filesystems
  ssh $ho "
    set -e;
    sudo apt-get install -y  apt-transport-https  curl  php-cli;
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg |sudo apt-key add -;
    echo 'deb http://apt.kubernetes.io/ kubernetes-xenial main' |sudo tee /etc/apt/sources.list.d/kubernetes.list;

    sudo apt-get update;
    sudo apt-get install -y  jq  xfsprogs;
    sudo apt-get install -y --allow-downgrades kubelet$VER  kubeadm$VER  kubectl$VER  kubernetes-cni$VER_CNI;

    sudo swapoff -a;
  "

  cat ${mydir?}/ports-unblock.sh | ssh $ho bash -ex
}


main "$@"
