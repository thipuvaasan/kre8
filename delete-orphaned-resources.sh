#!/bin/zsh -e

# Script to try to find kubernetes resources that may still be around and axe them.
# Probably `| tee FILE` the stdout and then `bash -x FILE` once you've manually reviewed it.
#
# Run this when you are `cd`-ed into the repo of interest.
#
# Example?  when a branch is deleted, its 'review app' related resources are supposed to go away.
# In our first few weeks of use, tracey found 3 such branches with resources still around.

mydir=${0:a:h}
source $mydir/aliases


function main() {
  delete-obsoleted-replicasets
  delete-resources-for-deleted-branches
}


function delete-resources-for-deleted-branches() {
  # Tries to remove 'review app' (etc.) resources still around for branches that have been deleted.
  mkdir .nix

  # NOTE: OK _some_ chars in branch names get 'slugged' here -- these are _registry_ names
  # _in reality_ that have been used as the closest branch proxy
  deployments-branches > .nix/k8

  cat .nix/k8 |cut -f2 |killspace |sort -u -o .nix/k8branches
  git branch -a| perl -pe 's=  remotes/origin/==' |killspace |sort -u -o .nix/gitbranches

  cat .nix/gitbranches |tr '/_.' '-' |tr A-Z a-z |sort -u -o .nix/gitbranches-slugged

  IFS=$'\n' # NEWLINE / ENTER / RETURN splitting, not SPACE
  for BRANCH in $(anotb .nix/k8branches .nix/gitbranches-slugged); do
    echo '#############################################################################'
    APP=$(grep -E "\t$BRANCH\$" .nix/k8 |cut -f1)
    echo "# KILLING RESOURCES FOR GONE BRANCH $BRANCH => $APP"
    for LINE in $(kall-everything 2>/dev/null |fgrep $APP |fgrep -v event/ |tr -s ' ' |cut -f1,2 -d' ' |sort -r); do
      if (echo "$LINE" |grep -qE "^ "); then
        echo kubectl delete $LINE \;
      else
        echo kubectl delete -n $LINE \;
      fi
    done
  done

  rm -rf .nix
}


function delete-obsoleted-replicasets() {
  # finds replicasets where DESIRED=0 CURRENT=0 READY=0 and axes them

  # iterate over list of namespaces w/ deployments
  # NOTE: this variant identifies some serverless functions w/ still temp un-cleaned out pods.
  #       ultimately, they come right back because they have active deployments, but..
  #   kc get ns --no-headers -o=custom-columns=NAME:.metadata.name
  for NS in $(kc get --all-namespaces deployments.extensions --no-headers --selector=tier=web -o=custom-columns=NAME:.metadata.namespace |sort -u); do
    for RS in $(kc get -n${NS?} replicaset.extensions |fgrep '0         0         0' |cut -f1 -d' '); do
      echo "# REMOVING NOT-NEEDED REPLICASET $RS"
      echo kubectl delete -n${NS?} replicaset/$RS \;
    done
  done
}


function anotb () {
  # limited diff of 2 sorted files - where we report lines in file 'A' not in file 'B'
	sort -c "$1"  &&  sort -c "$2"  &&  join -v 1 "$1" "$2"
}


function killspace () {
  # collapse whitespace to single [SPACE] chars and trim lead/trail whitespace
	perl -ne 's/\s*(\n)//; $ f = $ 1; s/^\s+//; s/\s+/ /g;  next unless m/./; print; print $ f ;'
}



main