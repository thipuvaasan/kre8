#!/bin/zsh -e

# Sets up Kubernetes cluster destined for GitLab Auto DevOps.
# Run this from your laptop.

mydir=${0:a:h}
source $mydir/aliases


function main() {
  [[ "$1" != '-'* ]]  &&  [[ "$#" -ge 2 ]]  ||  (
    echo "
  Usage: $0 [CNI: FLANNEL or WEAVE] [master hostname] <worker1 hostname> <worker2 hostname> ..
  "
    exit 1
  )


  set -x

  kubectl > /dev/null  ||  brew install kubernetes-cli   ||  echo "kubectl not installed"
  helm    > /dev/null  ||  brew install kubernetes-helm  ||  echo "helm not installed"

  mkdir -p $HOME/.kube


  # setup master first
  CNI=$1
  shift

  ho=$1
  shift
  $mydir/kubeadm-base.sh $ho
  $mydir/kubeadm-setup-master.sh $ho $CNI


  unset  KUBECONFIG
  for ho in $@; do
    echo "=== $ho ================================================================================"
    $mydir/kubeadm-base.sh $ho

    $mydir/kubeadm-join.sh  $ho
  done



  NUMBER_WORKERS=$(k8-workers |wc -l)

  [ "$NUMBER_WORKERS" = "0" ]  &&  (
    set +x
    echo -n '
  Single VM K8 cluster detected!

  By default, your cluster will not schedule pods on the master for security reasons.
  https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/

  ☸️  If you want to be able to schedule pods on the master, type yes here [yes or CTL-C]: '
    read cont
    set -x
    if [ "$cont" = "yes" ]; then
      set -x
      kubectl taint nodes --all node-role.kubernetes.io/master-
      set +x
    fi
  )



  # Setup Persistent Volume
  for ho in $(k8-master; k8-workers); do
    ssh $ho sudo mkdir -m777 -p ${PV_DIR?}
  done

  set +x
  if [ "$NUMBER_WORKERS" -le 1 ]; then
    echo '
  ☸️  ONE or TWO node cluster detected!

  Creating a single required PersistentVolume
  which will be usable by the only node that can run pods

  '
    set -x
    $mydir/persistent-volume-local.sh
  else
    $mydir/persistent-volume-ceph.sh
  fi
}


main "$@"
