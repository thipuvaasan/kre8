#!/bin/zsh -e


# Sets up a kubernetes cluster master, using industry-std `kubeadm`.


ho=${1:?"Usage: kubeadm-setup-master.sh <hostname> <optional 'FLANNEL' to use flannel instead of weave>"}


# Turn error => warning '/proc/sys/net/bridge/bridge-nf-call-iptables does not exist'.
# (esp.) If docker is 'too new' give user a chance to manually inspect errors and (interactively)
# tell us to retry ignoring failures or bail
typeset -a ARGS # array
ARGS=
ARGS+=(--ignore-preflight-errors=FileContent--proc-sys-net-bridge-bridge-nf-call-iptables)
[ "$2" = "FLANNEL" ]  &&  ARGS+=(--pod-network-cidr=10.244.0.0/16)

ssh ${ho?} sudo kubeadm init $ARGS


# Update your laptop's credentials to the new cluster credentials / config file.
# Will then be able to do cluster updates from your laptop.
rm -fv                                              $HOME/.kube/config
ssh ${ho?} sudo cat /etc/kubernetes/admin.conf  >|  $HOME/.kube/config
chmod 400                                           $HOME/.kube/config
unset  KUBECONFIG


# Pick networking plugin setup that is needed to get k8 DNS working
# PICK ONE!
if [ "$2" = "FLANNEL" ]; then
  # https://github.com/coreos/flannel
  kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
else
  # Verify client and server have same kubectl version
  ( kubectl version |egrep -o 'GitVersion:"[^"]+' |uniq -u |grep .  &&
    echo '
  uho - client and server kubectl versions differ -- please get them same (likely `brew upgrade kubernetes-cli`) and rerun..

  '
    exit 1
  )  ||  echo 'kubectl client and server version match ✅'

  kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version |base64 |tr -d '\n')"
fi

# we like the amazing dashboard
kubectl --namespace kube-system  delete deployments,services -l "app in (kubernetes-dashboard)"
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
