#!/bin/sh -e

# Makes it easier to retry a command w/ a timeout wrapper in Dockerfile during `docker build`
#
# Suggested way to use in Dockerfile repo:
#
# RUN curl https://gitlab.com/internetarchive/kre8/raw/master/retry > /bin/retry  &&  chmod +x /bin/retry
# RUN retry 5x 99s git clone ..

set +x
TIMES=$(echo "$1" |tr -dc '0-9')
shift
TIMEOUT=$(echo "$1" |tr -dc '0-9')
shift

# ugh, see if timeout _requires_ -t arg (atypical!) or not
ARG=
( timeout --help 2>&1 |grep -qF -- -t )  &&  ARG="-t"

(
  for i in $(seq 1 $TIMES); do
    /bin/echo -n " ( "
    /bin/echo -n echo "[$i/$TIMES]" timeout -s 9 $ARG $TIMEOUT "$@"
    /bin/echo -n "; "
    /bin/echo -n                    timeout -s 9 $ARG $TIMEOUT "$@"
    /bin/echo -n " ) || "
  done
  echo "exit 1"
) >| /tmp/.k8retry

sh -e /tmp/.k8retry  ||  exit 1
