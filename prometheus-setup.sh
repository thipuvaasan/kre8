#!/bin/zsh -e


# sets up existing prometheus from GitLab Auto DevOps to have public/grafana available metrics

mydir=${0:a:h}
source $mydir/aliases


# brute-force find IP address that is not: master, an ingress, load balancer, etc.
MASTER=$(IP $(k8-master))
echo '(non-master) list of cluster IP addresses:'
kc get nodes -o jsonpath='{.items[*].status.addresses[?(@.type=="InternalIP")].address}' |tr ' ' '\n' |fgrep -v ${MASTER?} |sort -u |tee .ips
echo '================='

echo 'IP addresses part of a service now:'
kc get --all-namespaces svc -o jsonpath='{.items[*].spec.externalIPs}' |tr -d '[]' |tr ' ' '\n' |sort -u |tee .svcs
echo '================='

# show
echo 'Available IPs:'
join -v 1 .ips .svcs
echo '================='


IP=$(join -v 1 .ips .svcs |head -1)
rm -f .ips .svcs

[ "${IP?}" = "" ]  &&  ( echo 'no IP avail'; exit 1 )

# pick non-ingress IP
kc expose service prometheus-prometheus-server -ngitlab-managed-apps --name=prometheus --type=NodePort --target-port=9090 --external-ip=${IP?}

open http://${IP?}/graph


# this is nice for grafana integration - pick 315 in new graph and plug in url from service above.
# https://grafana.com/grafana/dashboards/315