#!/bin/zsh -e

# Exposes one or more ports to the public to a running container into a given namespace.
# For example, if a 2nd daemon is running on port :8080 and a web browser needs to be able to
# talk to it directly.
#
# See 'mount-certs.sh' in this same dir if you need them to use https instead of http (likely!)

mydir=${0:a:h}

DEPLOY=auto-deploy-app

NS=${1:?"Usage: <namespace> <port> <port> .."}
XX=${2:?"Usage: <namespace> <port> <port> .."}
shift
PORTS="$@"

source $mydir/../aliases
set -x


for PORT in $(echo $PORTS); do
  # add port to be available to web browsers
  kc -n${NS?} patch deploy/production --type strategic --patch "
spec:
  template:
    spec:
      containers:
      - name: ${DEPLOY?}
        ports:
        - containerPort: ${PORT?}
          name: port-${PORT?}
          protocol: TCP
"
done


# expose port(s) to the world/public with a load balancer
# get IP address list from main ingress (will be 1+) and lightly reformat to valid JSON
INGRESS_IPS=$(kc get -ngitlab-managed-apps   service/ingress-nginx-ingress-controller --no-headers -o=custom-columns=NAME:.spec.externalIPs |perl -pe 's/ /","/g;   s/\[/["/;   s/\]/"]/;')

kc expose -n${NS?} deployment/production --type=LoadBalancer --name=porter
kc patch  -n${NS?} svc/porter -p='{"spec":{"externalIPs":'${INGRESS_IPS?}'}}'
