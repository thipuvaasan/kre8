#!/bin/bash -ex

# Setup a main Persistent Volume - that will appear at /pv inside a running container.
# (A 'PV' is a logical mounted disk that sticks around through pod restarts).

K8_NAMESPACE=${1:?"Usage: <namespace>"};
CLAIM_NAME=pv-claim
PVDIR=/pv

echo "
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: ${CLAIM_NAME?}
spec:
  accessModes:
    - ReadWriteMany
  volumeMode: Filesystem
  resources:
    requests:
      storage: 1Ti
" |kubectl -n"${K8_NAMESPACE?}" create -f -


kubectl patch -n"${K8_NAMESPACE?}" deploy/production --patch '{
  "spec":{
    "template": {
      "spec": {
        "volumes": [{
          "name": "pv",
          "persistentVolumeClaim": {
            "claimName": "'${CLAIM_NAME?}'"
          }
        }],
        "containers": [{
          "name": "auto-deploy-app",
          "volumeMounts": [
            {"mountPath": "'${PVDIR?}'", "name": "pv"}
          ]
        }]
      }
    }
  }
}'
