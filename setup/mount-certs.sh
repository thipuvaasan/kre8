#!/bin/zsh -ex

# Mounts letsencrypt https certs _into_ running container (at /etc/certs)
# in case a 2nd+ daemon in the container needs to talk https directly out a non-80/443 port.


alias kc=kubectl
DEPLOY=auto-deploy-app

NS=${1:?"Usage: <namespace>"}



# kc get secret/production-auto-deploy-tls -o "jsonpath={.data['tls\.crt']}"|base64 -D > tls.crt
# kc get secret/production-auto-deploy-tls -o "jsonpath={.data['tls\.key']}"|base64 -D > tls.key


kc -n${NS?} patch deploy/production --type strategic --patch "
spec:
  template:
    spec:
      volumes:
      - name: https-vol
        secret:
          secretName: production-auto-deploy-tls
      containers:
      - name: ${DEPLOY?}
        volumeMounts:
        - mountPath: /etc/certs
          name: https-vol
"
}
